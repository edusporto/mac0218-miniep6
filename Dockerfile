FROM caddy:2.5.1

COPY Caddyfile /etc/caddy/Caddyfile

# Uso de Volumes tira a necessidade desta parte
# COPY pages/* /usr/src/pages/